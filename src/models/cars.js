'use strict';
var mongoose   = require('mongoose')
  , ObjectId   = mongoose.Schema.Types.ObjectId;

var schema = mongoose.Schema({
  model: String,
  color: String,
});

module.exports = mongoose.model('Car', schema);
